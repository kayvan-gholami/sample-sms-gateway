<?php

namespace Tests\Feature;

use App\Jobs\SendSMSJob;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class SendMessageTest extends TestCase
{
    public function test_send_sms_endpoint()
    {
        $this->postJson(route('send-sms'), ['phone_number' => '09304660000', 'message' => 'test'])
            ->assertSuccessful()
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function test_send_sms_endpoint_with_invalid()
    {
        $this->postJson(route('send-sms'))
            ->assertUnprocessable()
            ->assertJsonStructure([
                'message',
                'errors'
            ]);
    }
}
