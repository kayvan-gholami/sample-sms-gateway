<?php

namespace App\Repositories;

use App\Models\SMS;
use App\Repositories\Contracts\SMSRepositoryInterface;

class SMSRepository extends BaseRepository implements  SMSRepositoryInterface
{
    public function __construct(SMS $model)
    {
        parent::__construct($model);
    }
}
