<?php

namespace App\Repositories\Contracts;

interface BaseRepositoryInterface
{
    public function create(array $attributes);
    public function update(array $attributes, int $id);
    public function all($columns = array('*'), string $orderBy = 'id', string $sortBy = 'desc');
    public function find(int $id);
    public function delete(int $id);
    public function getModel();
    public function paginate($prePage=15);
}
