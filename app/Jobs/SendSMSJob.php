<?php

namespace App\Jobs;

use App\Repositories\SMSRepository;
use App\Services\SMS\Facades\SMS;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendSMSJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private string $phone_number;
    private string $message;

    /**
     * Create a new job instance.
     *
     * @param string $phone_number
     * @param string $message
     */
    public function __construct(string $phone_number, string $message)
    {
        $this->onQueue('sms');
        $this->phone_number = $phone_number;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SMSRepository $repository)
    {
        try {
            // via() method is available for send with another drivers
            SMS::send($this->message, function($sms) {
                $sms->to($this->phone_number);
            });
            $repository->create([
                'phone_number' => $this->phone_number,
                'message' => $this->message,
                'via' => config('sms.default'),
                'status' => 'sent',
            ]);
            dump('asds');
        } catch (\Exception $e) {
            dump('zzz');
            $this->fail($e);
            Log::error($e->getMessage());
        }
    }
}
