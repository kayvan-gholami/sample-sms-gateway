<?php

namespace App\Services\SMS;

use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    public function boot()
    {
        $this->app->bind('sms', function () {
            return new SMS(config('sms'));
        });
    }
}
