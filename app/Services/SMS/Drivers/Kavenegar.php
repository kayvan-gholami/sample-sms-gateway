<?php

namespace App\Services\SMS\Drivers;

use App\Services\SMS\Contracts\Driver;
use Kavenegar\KavenegarApi;

class Kavenegar extends Driver
{
    protected KavenegarApi $client;

    protected function boot(): void
    {
        $this->client = new KavenegarApi(data_get($this->settings, 'apiKey'));
    }

    public function send()
    {
        $response = collect();

        foreach ($this->recipients as $recipient) {
            $this->client->send(data_get($this->settings, 'from'), $recipient, $this->body);
        }

        return (count($this->recipients) == 1) ? $response->first() : $response;
    }
}
