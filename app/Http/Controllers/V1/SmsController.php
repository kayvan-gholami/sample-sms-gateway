<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\SendRequest;
use App\Jobs\SendSMSJob;
use App\Repositories\Contracts\SMSRepositoryInterface;
use App\Repositories\SMSRepository;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    private $repository;

    public function __construct(SMSRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $messages = $this->repository->paginate();
        return response()->json($messages, 200);
    }

    public function send(SendRequest $request)
    {
        SendSMSJob::dispatch($request->phone_number, $request->message)
            ->onQueue('sms');

        return response()->json(['message'=>'The message was placed in the sending queue'], 200);
    }
}
