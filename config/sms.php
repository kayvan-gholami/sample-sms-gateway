<?php

use App\Services\SMS\Drivers\Kavenegar;
use App\Services\SMS\Drivers\Ghasedak;

return [


    'default' => 'kavenegar',


    'drivers' => [
        'kavenegar' => [
            'apiKey' => '7364532F4C6F4F6F4C44692B6830784F4350425859627666506D547763314159',
            'from' => '1000770880'
        ],
        'ghasedak' => [
            'url' => 'http://api.iransmsservice.com',
            'apiKey' => '',
            'from' => '',
        ]
    ],


    'map' => [
        'kavenegar' => Kavenegar::class,
        'ghasedak' => Ghasedak::class,
    ],
];
